<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function test() {

        $data = "ayo belajar";
        $pdf = PDF::loadView('pdf.test', compact('data'));
        return $pdf->download('test.pdf');
    }
}
